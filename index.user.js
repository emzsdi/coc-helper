// ==UserScript==
// @name CoC helper
// @namespace https://www.codingame.com/
// @version 0.4
// @description Log ClashOfCode problem description
// @match https://www.codingame.com/*
// @grant unsafeWindow
// @grant GM_getValue
// @grant GM_setValue
// @grant GM_log
// @connect api.telegram.org
// @run-at document-idle
// ==/UserScript==

(function() {
    'use strict';
    const log = (...args) => GM_log('coc', ...args);
    const _wr = function(type) {
        const orig = unsafeWindow.history[type];
        return function() {
            const rv = orig.apply(this, arguments);
            const e = new Event(type);
            e.arguments = arguments;
            unsafeWindow.dispatchEvent(e);
            return rv;
        };
    };
    unsafeWindow.history.pushState = _wr('pushState');
    unsafeWindow.history.replaceState = _wr('replaceState');
    const $http = unsafeWindow.angular.element(document.body).injector().get('$http');
    const origPost = $http.post;
    const responseHandlers = {};
    let user = {};
    const leaderboard = new Map();
    $http.post=function(...args){
        const res = origPost.call(this, ...args);
        const path = (typeof args[0])=='string' ? args[0] : args[0].path;
        const data = args[1];
        return res.then((res)=>{
            switch(path) {
                case '/services/Leaderboards/getCodinGamerClashRanking':
                    user = res.data.codingamer;
                    processLeaderboard();
                    break;
                case '/services/ClashOfCode/findClashByHandle':
                    res.data.players = res.data.players.map((p) => {
                        const pr = leaderboard.get(p.codingamerHandle);
                        if(pr) {
                            p.codingamerNickname = `(${pr.globalRank}) ${p.codingamerNickname}`;
                        }
                        return p;
                    })
                    break;
                case '/services/ClashOfCode/findClashReportInfoByHandle':
                    try {
                        log({path, data, res})
                        return res;
                    }
                    finally {
                        setTimeout(()=>{
                            if(document.location.pathname.indexOf('/ide/')!=-1) {
                                const container = document.querySelector('.code-management .solutions');
                                if(container) {
                                    container.innerHTML = res.data.players.filter(p=>p.solutionShared).map(
                                        p => `<a target="_blank" href="https://www.codingame.com/report/${p.testSessionHandle}"><small>${p.codingamerNickname}</small></a>`
                                    ).join('\n');
                                }
                            }
                            if(document.location.pathname.indexOf('/clash/report/')!=-1) {
                                const players = res.data.players.reduce((m,p)=>{
                                    m.set(p.codingamerHandle, p);
                                    return m;
                                },new Map())
                                log(players);
                                for(const pl of document.querySelectorAll('.player-report')){
                                    const id = pl.querySelector('a.avatar-container').attributes['ng-href'].value.slice(9);
                                    const cp = players.get(id);
                                    log(cp)
                                    if(cp.solutionShared) {
                                        const el = document.createElement('a');
                                        el.setAttribute('href', `/report/${cp.testSessionHandle}`);
                                        el.setAttribute('target', '_blank');
                                        el.textContent = '👁';
                                        pl.appendChild(el);
                                    }
                                }
                                //player-report
                            }
                        },0);
                    }
                    break;
                case '/services/TestSession/startTestSession':
                    //question-infos
                    try {
                        log({path, data, res,_:res.data.currentQuestion});
                        return res;
                    }
                    finally {
                        setTimeout(()=>{
                            const el = document.createElement('h4');
                            el.textContent = res.data.currentQuestion.question.title;
                            log(document.querySelector('.question-infos'),el)
                            document.querySelector('.question-infos').appendChild(el);
                        },0);
                    }
                    break;
            }
            log({path, data, res})
            return res;
        });
    };

    const TG_TOKEN = getEnv('TG_TOKEN', 'Telegram bot token');
    const TG_CHANNEL_ID = getEnv('TG_CHANNEL_ID', 'Telegram channel id');
    log({TG_TOKEN,TG_CHANNEL_ID})

    const clashPageRegex = /^\/clashofcode\/clash\/([0-9a-f]{32,})$/;

    let lastClash = null;

    const onNavigation = async () => {
        try {
            const path = window.location.pathname;
            const clash = path.match(clashPageRegex);
            log({path, lastClash});
            if(clash) {
                lastClash = {id:clash[1]};
            }
            else if(path.indexOf('/ide/')===0) {
                log({lastClash});
                await waitForElement('.statement-bloc');
                await sleep(700);
                initEditorHelpers();
                const isTestUser = ['Iwakura.Lain','Karasik'].some(u=>u==getUserName());
                if(lastClash && !isTestUser) {
                    lastClash.statement = document.querySelector('.statement-bloc').innerHTML;
                    const msg = formatMessage(lastClash);
                    log(msg)
                    await sendMessage(msg);
                }
            }
            else if(lastClash && lastClash.statement && path.includes(`/clash/report/${lastClash.id}`)) {
                await waitForElement('.button-container');
                const cgGreen = '#56BC58';
                const buttonContainer = document.querySelector('.content-container .button-container');
                const button = document.createElement('button');
                button.textContent = 'problem statement';
                button.classList.add('find-clash-button');
                button.style = `background: ${cgGreen}`;
                button.addEventListener('click', () => showPopup(lastClash.statement));
                buttonContainer.appendChild(button);
            }
        }
        catch(err) {
            log(err);
        }
    };
    unsafeWindow.onpopstate = onNavigation;
    unsafeWindow.addEventListener('pushState', onNavigation);
    unsafeWindow.addEventListener('replaceState', onNavigation);

    setTimeout(onNavigation, 500);
    function showPopup(html) {
        const popup = document.createElement('div');
        popup.classList.add('popup');
        popup.style='max-width: 600px';
        popup.innerHTML = html;
        const container = document.createElement('div');
        container.classList.add('popup-container');
        container.appendChild(popup);
        popup.addEventListener('click', (e) => e.stopPropagation());
        const wrapper = document.createElement('div');
        wrapper.classList.add('cg-popup');
        wrapper.appendChild(container);
        container.addEventListener('click', () => document.body.removeChild(wrapper));
        document.body.appendChild(wrapper);
    }
    async function sendMessage(text){
        if(!TG_TOKEN) {
            log('Skipped posting message. Token is not defined');
            return;
        }
        const url = `https://api.telegram.org/bot${TG_TOKEN}/sendMessage`;
        const options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({
                chat_id: TG_CHANNEL_ID,
                parse_mode: 'MarkdownV2',
                disable_web_page_preview: true,
                text,
            }),
        };
        const response = await(await fetch(url, options)).json();
        log({response});
    }
    function initEditorHelpers() {
        const ctrl = angular.element('cg-ide-code-editor').controller('cgIdeCodeEditor');
        log(ctrl)
        const editor = document.querySelector('.inputarea');
        const stBtn = '';
        const lowered = [];
        const lower = (name) => name.toUpperCase()==name?(lowered.push(name),name.toLowerCase()):name
        const fCom = () => {
            let txt = ctrl.code
            .replace(/\s*\/\*.*?\*\/\s*?\n/gs,'')
            .replace(/\s*\/\/.*/gm,'')
            .replace(/^\s*#.*/gm,'')
            .replace(/\n\n+/g,'\n\n')
            .replace(/;\n/g,'\n')
            .replace(/\bconsole\.log\b/g, 'print')
            .replace(/\.split\('(.*?)'\)/g, '.split\`$1\`')
            .replace(/\bconst\b/g, 'let')
            .replace(/^\s*var inputs = readline\(\).split` `;(?:\n.*?inputs\[.*?$)+/gm, (s) => {
                const params = s.match(/(?<=(?:var|let|const) )(.*?)\b = .*?inputs\[/g);
                log(params);
                const paramNames = params.map(p=>p.match(/^\w+/)[0]).map(lower);
                const numOnly = params.every(p=>p.includes('parse'));
                return `let [${paramNames.join(', ')}] = readline().split\` \`${numOnly?'.map(n=>+n)':''}`
                //return s;
            })
            .replace(/gets.split\(" "\)\.collect \{\|x\| x\.to_i\}/g, 'gets.split.map(&:to_i)')
            .replace(/(?:var|let|const) (.*?)\b = parse(?:Int|Float)\((.*)\)/g, (s,name,arg) => `let ${lower(name)} = +${arg}`)
            .replace(new RegExp(String.raw`\b(${lowered.join('|')})\b`,'g'), s => s.toLowerCase())
            .trim()+'\n'
            ctrl.code = txt;
        };
        setTimeout(fCom, 0);
        document.querySelector('.code-management').appendChild((() => {
            const btn = document.createElement('button');
            btn.innerText = 'Com';
            btn.classList.add('coc-bnt');
            btn.onclick = fCom;
            return btn;
        })());

        document.querySelector('.code-management').appendChild((()=>{
            const btn = document.createElement('button');
            btn.innerText = 'Min';
            btn.classList.add('coc-bnt');
            btn.onclick = () => {
                let txt = ctrl.code
                    //.replace(/` `/g, '`п`')
                    .replace(/^(?<!`) +/gm, '')
                    .replace(/(?<=[\w\)]) *([,:\?\=\(\{]) *(?=[\w\+])/g, '$1')
                    .replace(/;\s*\n/g, '\n')
                    //.replace(/`п`/g, '` `')
                    .trim()+'\n'
                const rl = /readline\(\)/g;
                if(txt.match(rl)?.length>1) {
                    txt = 'r=readline\n' + txt.replace(rl, 'r()')
                }
                ctrl.code = txt;
            }
            return btn;
        })());

        const helpers = {
            sum: `a=>a.reduce((s,v)=>+v+s,0)`,
            mul: `a=>a.reduce((a,b)=>a*b)`,
            lower: `s=>s.toLowerCase()`,
            upper: `s=>s.toUpperCase()`,
            //trim: String.raw`s=>s.replace(/\s/g,'')`,
            ord: `c=>c.charCodeAt()`,
            chr: `c=>String.fromCharCode(c)`,
            gcd: `(a,b)=>a?gcd(b%a,a):b`,
            lcm: `(a,b)=>Math.abs(a*b)/gcd(a,b)`,
            droot: `_=s=>_=[...''+s].reduce((s,v)=>+v+s,0),_>9?droot(_):_`,
            //pad: `pad=(v,l,p)=>(v+'').padStart(l,p)`,
            range: `n=>[...Array(n).keys()]`,
            loop: `function*(...i){if(i.length)for(let v of i[0])if(i.length==1)yield[v];else for(let n of loop(...i.slice(1)))yield[v,...n]}`,
            subsets: `function*(a){for(let i=0,l=1<<a.length;++i<l;){let r=[];for(let j=0;j<a.length;j++)if(i&(1<<j))r.push(a[j]);yield r}}`,
            permut: `function*(a){if(a.length<2)for(let v of a)yield[v];else for(let i=0;i<a.length;i++)for(let n of permut([...a.slice(0,i),...a.slice(i+1)]))yield[a[i], ...n]}`,
            binSearch: `(l,r,c)=>{if(l>=r) return l;let m=l+r>>1, d=c(m);return (d<-1e-7)?binSearch(l,m-1,c):(d>1e-7)?binSearch(m+1,r,c):m;}`,
            //sdist: `sdist=(x0,y0,x1,y1)=>(x1-x0)**2+(y1-y0)**2`,
            xor: `a=>a.reduce((a,b)=>a^b)`,
            hex: `n=>(+n).toString(16)`,
            bin: `n=>(+n).toString(2)`,
            cmp: `(a,b)=>a-b`,
            avg: `a=>Math.round(sum(a)/a.length)`,
            group: `a=>a.reduce((s,v)=>(s[v]=(s[v]||0)+1,s),{})`,
            count: `(s,r)=>[...s.matchAll(r)].length`,
            uniq: `a=>[...new Set(a)].sort().join('')`,
            las: String.raw`s=>s.replace(/(.)\1*/g, s=>s.length+s[0])`,
            transpose: `a=>{let r=[],i=0;for(;i<(a[0]||[]).length;i++)r.push(a.map(v=>v[i]).reverse());return r;}`,
            ff: `(n,f,s)=>n<f?ff(n+s,f,s):(n-f)%s+f`,
            dm: `(n,a)=>a.reduceRight((a,v,_)=>(_=a.pop(),a.push((_-a[a.push(_%v)-1])/v),a), [n]).reverse()`,
            inner: `(a,...b)=>[...a].filter(v=>b.every(l=>l.has(v)))`,
            outer: `(a,...b)=>[...a].filter(v=>!b.some(l=>l.has(v)))`,
            write: `process.stdout.write.bind(process.stdout)`,
            //undup: String.raw`a=>a.replace(/(.)\1+/,'$1')`,
            // splitAt, chunk
            // money/time to cents/seconds and back
            // backtracking helper
        };
        const fNamePattern = '('+Object.keys(helpers).join('|')+')';
        const findUsages = (code) => [...new Set(code.match(new RegExp(String.raw`\b${fNamePattern}(?=[\(\)])`,'g'))||[])]
        const updateDefinitions = (code, flist) => {
            for(;;){
                let d=false;
                for(const f of flist) {
                    for(const sub of findUsages(helpers[f])) {
                        if(!flist.includes(sub)) {
                            d=true;
                            flist.unshift(sub);
                        }
                    }
                }
                if(!d) break;
            }
            return [...flist.map(f=>f+'='+helpers[f]), code.replace(new RegExp(String.raw`^${fNamePattern}=.*?\n`,'gm'), '')].join('\n');
        }

        document.querySelector('.code-management').appendChild((()=>{
            const btn = document.createElement('button');
            btn.innerText = 'Tst';
            btn.classList.add('coc-bnt');
            btn.onclick = () => {
                let txt = updateDefinitions(ctrl.code, findUsages(ctrl.code))
                ctrl.code = txt;
                setTimeout(()=>$('.play-all-testcases').click(), 10);
            }
            return btn;
        })());

        document.querySelector('.code-management').appendChild((()=>{
           const list = document.createElement('div');
           list.classList.add('coc-list');
           const btn = document.createElement('div');
           btn.innerText = 'snip';
           btn.classList.add('coc-bnt');
           btn.appendChild(list);
           for(const name in helpers){
               const btn = document.createElement('button');
               btn.classList.add('coc-li');
               btn.innerText = name;
               btn.onclick = () => ctrl.code = name+'='+helpers[name] + '\n' + ctrl.code;
               list.appendChild(btn);
           }
           return btn;
        })());

        document.querySelector('.code-management').appendChild((() => {
            const div = document.createElement('div');
            div.classList.add('solutions');
            return div;
        })());
    }
    function getEnv(name, description){
        let value = GM_getValue(name);
        if(!value) {
            value = unsafeWindow.prompt(description);
            if(value) {
                GM_setValue(name, value);
            }
        }
        return value;
    }
    function formatMessage({id}) {
        const header = `*User*: _${escapeMD(getUserName())}_; *Mode*: _${getProblemMode()}_; [${id}](https://www.codingame.com/clashofcode/clash/report/${id})\n`;
        return limitLength([header], escapeMD(getProblemDescription()));
    }
    function limitLength([a='', b=''], content){
        const maxLen = 4090 - a.length - b.length;
        return a+content.substr(0, maxLen)+b;
    }
    function getProblemDescription() {
        const statementBlock = document.querySelector('.statement-bloc');
        if(!statementBlock) return;
        return [...statementBlock.children].filter(c=>!c.classList.contains('cg-ide-contributor')).map(e=>e.innerText).join`\n`;
    }
    function getProblemMode() {
        const title = document.querySelector('[ng-bind="api.title"]')?.innerText;
        if(!title) return 'Unknown';
        return title.split(' - ')[1].split(' ')[0];
    }
    function getUserName() {
        return unsafeWindow.codingame.session.codinGamer.pseudo;
    }
    function escapeMD(text) {
        //'_', '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!'
        return text.replace(/[_\*\[\]\(\)~`>#\+-=|\{\}\.!]/g, s=>'\\'+s)
    }
    async function waitForElement(query, timeout = 10000) {
        let delay = 100;
        const startTime = Date.now();
        do {
            const element = document.querySelector(query);
            if(element) return element;
            await sleep(delay);
            delay *= 2;
        } while(Date.now() + delay < startTime + timeout);
        throw new Error(`Element '${query}' not found`);
    }
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async function processLeaderboard() {
        const {data} = await origPost(
            '/services/Leaderboards/getClashLeaderboard',
            [1, {keyword:'',active:false,column:'',filter:''},user.publicHandle,true,'global',null]
        );
        for(const user of data.users) {
            leaderboard.set(user.codingamer.publicHandle, user);
        }
    }

    (() => {
        const style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = `
            .coc-bnt{margin: 0 10px;color:#b3b9ad;border: solid 1px #999;padding: 0px 5px;border-radius: 3px;background: rgba(0,0,0,0.3);position: relative;cursor:pointer;}
            .coc-bnt .coc-list{display: none}
            .coc-bnt:hover .coc-list{display: block;position:absolute;left:-3px;background:rgba(50,50,50,0.9);border:solid 1px #333;z-index:10;column-count:2;column-gap:0;}
            .coc-li{cursor: pointer;padding:5px 10px;display:block;width:100%;}
            .coc-li:hover{background: #555;}
        `;
        document.getElementsByTagName('head')[0].appendChild(style);
    })();
})();